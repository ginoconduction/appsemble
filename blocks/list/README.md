A block that can be used to display a vertically oriented list of data.

### Images

![Table screenshot](https://gitlab.com/appsemble/appsemble/-/raw/0.18.10/docs/images/list.png)
