A block that fetches data and emits it using the events API.

This can be used to provide data to other blocks.
