image: node:14-slim

stages:
  - test
  - build
  - publish
  - deploy
  - provision
  - end 2 end

workflow:
  rules:
    # By default, run jobs for every merge request.
    - if: $CI_MERGE_REQUEST_ID
    # By default, run jobs for every commit on master.
    - if: $CI_COMMIT_BRANCH == 'master'
    # By default, run jobs for every schedule
    - if: $CI_PIPELINE_SOURCE == 'schedule'
    # By default, run jobs for every tag
    - if: $CI_COMMIT_TAG

###################################################################################################
#  Job Templates                                                                                  #
###################################################################################################

# A preset for running Docker in Docker.
.docker:
  interruptible: true
  services:
    - docker:dind
  image: docker
  dependencies: []
  before_script:
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY

# A preconfigured environment for using Helm.
.helm:
  image: dtzar/helm-kubectl:3
  dependencies: []

# A preconfigured environment for using Yarn.
.yarn:
  interruptible: true
  dependencies: []
  before_script:
    - yarn --frozen-lockfile --ignore-scripts

###################################################################################################
#  Test Stage                                                                                     #
###################################################################################################

# Lint JavaScript code using ESLint.
eslint:
  extends: .yarn
  script:
    - yarn eslint --format gitlab .
  artifacts:
    reports:
      codequality: gl-codequality.json

# Lint Helm charts.
helm lint:
  extends: .helm
  script:
    - helm lint config/charts/*

# Verify app messages are in sync with the app definition.
i18n:
  extends: .yarn
  script:
    - yarn appsemble app extract-messages --verify nl apps/*

# Check formatting using prettier.
prettier:
  extends: .yarn
  script:
    - yarn prettier .

# Lint Markdown using remark.
remark lint:
  extends: .yarn
  script:
    - yarn remark --frail --no-stdout .

# Lint CSS using stylelint.
stylelint:
  extends: .yarn
  script:
    - yarn stylelint .

# Run unittests using NodeJS 12.
test node 12:
  interruptible: true
  image: node:12-slim
  services:
    - name: postgres:11
  variables:
    POSTGRES_DB: testAppsemble
    POSTGRES_USER: admin
    POSTGRES_PASSWORD: password
    DATABASE_URL: 'postgres://admin:password@postgres:5432/testAppsemble'
  script:
    - yarn --frozen-lockfile
    - yarn jest
  artifacts:
    reports:
      junit: junit.xml

# Run unittests using NodeJS 14.
test node 14:
  interruptible: true
  services:
    - name: postgres:11
  variables:
    POSTGRES_DB: testAppsemble
    POSTGRES_USER: admin
    POSTGRES_PASSWORD: password
    DATABASE_URL: 'postgres://admin:password@postgres:5432/testAppsemble'
  script:
    - yarn --frozen-lockfile
    - yarn jest --coverage
    - yarn codecov
  artifacts:
    reports:
      junit: junit.xml
      cobertura: coverage/cobertura-coverage.xml

# Check type validity for our TypeScript files.
tsc:
  extends: .yarn
  script:
    - yarn workspaces run tsc

# Check type validity for our TypeScript files.
validate:
  extends: .yarn
  script:
    - yarn scripts validate

###################################################################################################
#  Build Stage                                                                                    #
###################################################################################################

# Build the Docker image.
build docker image:
  extends: .docker
  stage: build
  needs: []
  script:
    - docker build -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME" .
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"

# Build the npm packages that should be published.
pack:
  extends: .yarn
  stage: build
  needs: []
  script:
    - yarn workspace @appsemble/types pack
    - yarn workspace @appsemble/sdk pack
    - yarn workspace @appsemble/preact pack
    - yarn workspace @appsemble/node-utils pack
    - yarn workspace @appsemble/utils pack
    - yarn workspace @appsemble/webpack-config pack
    - yarn workspace @appsemble/cli pack
    - yarn workspace appsemble pack
    - mkdir build/
    - find packages -name '*.tgz' -exec mv {} ./build/ \;
  artifacts:
    name: npm packages
    expose_as: packages
    paths:
      - build/

# Package the Helm chart
helm package:
  extends: .helm
  stage: build
  needs: []
  script:
    - helm repo add bitnami https://charts.bitnami.com/bitnami
    - cp LICENSE.md config/charts/appsemble/
    - helm dependency build config/charts/appsemble
    - helm package -d public config/charts/appsemble/
  artifacts:
    name: Helm chart
    expose_as: Helm chart
    paths:
      - public/

###################################################################################################
#  Publish Stage                                                                                  #
###################################################################################################

# Publish the Docker image that was built to Docker Hub.
publish docker:
  extends: .docker
  stage: publish
  needs:
    - build docker image
  variables:
    GIT_STRATEGY: none
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo $DOCKER_HUB_PASSWORD | docker login -u $DOCKER_HUB_USERNAME --password-stdin
    - docker pull "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG"
    - docker tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG" "$CI_REGISTRY_IMAGE:latest"
    - docker tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG" "appsemble/appsemble:latest"
    - docker tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG" "appsemble/appsemble:$CI_COMMIT_TAG"
    - docker push "$CI_REGISTRY_IMAGE:latest"
    - docker push "appsemble/appsemble:latest"
    - docker push "appsemble/appsemble:$CI_COMMIT_TAG"

publish npm:
  stage: publish
  dependencies:
    - pack
  needs:
    - pack
  variables:
    GIT_STRATEGY: none
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - npm config set //registry.npmjs.org/:_authToken "$NPM_TOKEN"
    - find . -exec npm publish --access public {} \;

# Perform various post release actions.
sentry release:
  image:
    name: getsentry/sentry-cli:1
    entrypoint: [/bin/sh, -c]
  stage: publish
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - sentry-cli releases new --finalize "$CI_COMMIT_TAG"
    - sentry-cli releases set-commits --auto "$CI_COMMIT_TAG"

###################################################################################################
#  Deploy Stage                                                                                   #
###################################################################################################

# Delete all succesful jobs for review environments nightly
cleanup k8s jobs:
  extends: .helm
  stage: deploy
  rules:
    - if: $CI_PIPELINE_SOURCE == 'schedule'
  environment:
    name: review/cleanup
  script:
    - kubectl delete job $(kubectl get job -o=jsonpath='{.items[?(@.status.succeeded==1)].metadata.name}')

# Deploy the Docker image for a branch to a review environment.
review:
  extends: .helm
  stage: deploy
  needs:
    - build docker image
  rules:
    - if: $CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH == 'appsemble/appsemble'
  environment:
    name: review/$CI_MERGE_REQUEST_IID
    url: https://$CI_MERGE_REQUEST_IID.appsemble.review
    on_stop: stop review
  script:
    # This is the only way to make sure the review environment can be overwritten
    - helm delete "review-$CI_MERGE_REQUEST_IID" || true
    - helm repo add bitnami https://charts.bitnami.com/bitnami
    - helm dependency build config/charts/appsemble
    - helm install "review-$CI_MERGE_REQUEST_IID" config/charts/appsemble
      --atomic
      --set "fullnameOverride=review-$CI_MERGE_REQUEST_IID"
      --set "gitlab.app=$CI_PROJECT_PATH_SLUG"
      --set "gitlab.env=$CI_ENVIRONMENT_SLUG"
      --set "image.pullPolicy=Always"
      --set "image.repository=$CI_REGISTRY_IMAGE"
      --set "image.tag=$CI_COMMIT_REF_NAME"
      --set "ingress.annotations.cert-manager\.io/cluster-issuer=letsencrypt-dev"
      --set "ingress.annotations.nginx\.ingress\.kubernetes\.io/proxy-body-size=50m"
      --set "ingress.enabled=true"
      --set "ingress.host=${CI_ENVIRONMENT_URL/https:\/\//}"
      --set "ingress.tls.secretName=review-$CI_MERGE_REQUEST_IID-tls"
      --set "ingress.tls.wildcardSecretName=review-$CI_MERGE_REQUEST_IID-tls-wildcard"
      --set "migrateTo=next"
      --set "global.postgresql.existingSecret=postgresql-secret"
      --set "provision.account.clientCredentials=$APPSEMBLE_CLIENT_CREDENTIALS"
      --set "provision.account.email=$BOT_ACCOUNT_EMAIL"
      --set "provision.account.name=$BOT_ACCOUNT_NAME"
      --set "provision.account.password=$BOT_ACCOUNT_PASSWORD"
      --set "postgresql.fullnameOverride=review-$CI_MERGE_REQUEST_IID-postgresql"
      --set "proxy=true"
      --set "sentryEnvironment=$CI_ENVIRONMENT_NAME"
      --set "sentrySecret=sentry"

# Stop a review environment.
stop review:
  extends: .helm
  stage: deploy
  rules:
    - if: $CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH == 'appsemble/appsemble'
      when: manual
  allow_failure: true
  environment:
    name: review/$CI_MERGE_REQUEST_IID
    action: stop
  variables:
    GIT_STRATEGY: none
  script:
    - helm delete "review-$CI_MERGE_REQUEST_IID"

# Deploy the Docker image for master to the staging environment.
staging:
  extends: .helm
  stage: deploy
  needs:
    - build docker image
  rules:
    - if: $CI_PIPELINE_SOURCE == 'schedule'
  environment:
    name: staging
    url: https://staging.appsemble.review
  script:
    # This is the only way to make sure staging can be overwritten
    - helm delete staging || true
    - helm repo add bitnami https://charts.bitnami.com/bitnami
    - helm dependency build config/charts/appsemble
    # First, install from the last stable tag.
    - helm install staging config/charts/appsemble
      --atomic
      --set "gitlab.app=$CI_PROJECT_PATH_SLUG"
      --set "gitlab.env=$CI_ENVIRONMENT_SLUG"
      --set "global.postgresql.existingSecret=postgresql-secret"
      --set "image.pullPolicy=Always"
      --set "image.repository=$CI_REGISTRY_IMAGE"
      --set "image.tag=master"
      --set "ingress.annotations.cert-manager\.io/cluster-issuer=letsencrypt-dev"
      --set "ingress.annotations.nginx\.ingress\.kubernetes\.io/proxy-body-size=50m"
      --set "ingress.enabled=true"
      --set "ingress.host=staging.appsemble.review"
      --set "ingress.tls.secretName=staging-tls"
      --set "ingress.tls.wildcardSecretName=staging-tls-wildcard"
      --set "migrateTo=next"
      --set "oauthSecret=oauth2"
      --set "postgresql.fullnameOverride=staging-postgresql"
      --set "provision.account.clientCredentials=$APPSEMBLE_CLIENT_CREDENTIALS"
      --set "provision.account.email=$BOT_ACCOUNT_EMAIL"
      --set "provision.account.name=$BOT_ACCOUNT_NAME"
      --set "provision.account.password=$BOT_ACCOUNT_PASSWORD"
      --set "proxy=true"
      --set "sentryEnvironment=staging"
      --set "sentrySecret=sentry"

production:
  extends: .helm
  stage: deploy
  needs:
    - publish docker
  rules:
    - if: $CI_COMMIT_TAG
  dependencies: []
  environment:
    name: production
    url: https://appsemble.app
  script:
    - helm repo add bitnami https://charts.bitnami.com/bitnami
    - helm dependency build config/charts/appsemble
    - helm upgrade appsemble config/charts/appsemble
      --set "gitlab.app=$CI_PROJECT_PATH_SLUG"
      --set "gitlab.env=$CI_ENVIRONMENT_SLUG"
      --set "global.postgresql.existingSecret=$POSTGRESQL_SECRET"
      --set "global.postgresql.postgresqlDatabase=$POSTGRESQL_DATABASE"
      --set "global.postgresql.postgresqlUsername=$POSTGRESQL_USERNAME"
      --set "global.postgresql.servicePort=$POSTGRESQL_PORT"
      --set "ingress.annotations.cert-manager\.io/cluster-issuer=letsencrypt-prod"
      --set "ingress.annotations.nginx\.ingress\.kubernetes\.io/proxy-body-size=50m"
      --set "ingress.enabled=true"
      --set "ingress.host=appsemble.app"
      --set "ingress.tls.secretName=appsemble-tls"
      --set "ingress.tls.wildcardSecretName=appsemble-tls-wildcard"
      --set "oauthSecret=oauth2"
      --set "postgresql.fullnameOverride=$POSTGRESQL_HOST"
      --set "postgresql.enabled=false"
      --set 'postgresSSL=true'
      --set "proxy=true"
      --set "sentryEnvironment=production"
      --set "sentrySecret=sentry"
      --wait

# Perform various post release actions.
post release:
  extends: .yarn
  stage: deploy
  # This is new and mainly used to update nice-to-have metadata.
  # Allow failure to prevent blocking of important jobs.
  allow_failure: true
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - yarn scripts post-release

###################################################################################################
#  Provision Stage                                                                                #
###################################################################################################

# Provision the review environment with the blocks that were built in the build stage.
provision review:
  extends: .yarn
  stage: provision
  rules:
    - if: $CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH == 'appsemble/appsemble'
  needs:
    - review
  script:
    - yarn appsemble config set context review
    - yarn appsemble config set remote "https://$CI_MERGE_REQUEST_IID.appsemble.review"
    - yarn appsemble organization create appsemble --name Appsemble --email support@appsemble.com --website https://appsemble.com --description 'The open source low-code app building platform'
    - yarn appsemble -vv block publish blocks/*
    - yarn appsemble -vv app create apps/*

# Provision the staging environment with the blocks that were built in the build stage.
provision staging:
  extends: .yarn
  stage: provision
  rules:
    - if: $CI_PIPELINE_SOURCE == 'schedule'
  needs:
    - staging
  script:
    - yarn appsemble config set context staging
    - yarn appsemble config set remote https://staging.appsemble.review
    - yarn appsemble organization create appsemble --name Appsemble --email support@appsemble.com --website https://appsemble.com --description 'The open source low-code app building platform'
    - yarn appsemble -vv block publish blocks/*
    - yarn appsemble -vv app create apps/*

# Provision the production environment with the blocks that were built in the build stage.
provision production:
  extends: .yarn
  stage: provision
  rules:
    - if: $CI_COMMIT_TAG
  needs:
    - production
  environment:
    name: production provision
  script:
    - yarn appsemble config set context production
    - yarn appsemble config set remote https://appsemble.app
    - yarn appsemble config set ignore-conflict true
    - yarn appsemble -vv block publish blocks/*
    - yarn appsemble -vv app update --force apps/*

###################################################################################################
#  End to End Stage                                                                               #
###################################################################################################

# Test template apps using Lighthouse.
lighthouse:
  interruptible: true
  stage: end 2 end
  dependencies: []
  rules:
    - if: $CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH == 'appsemble/appsemble'
    - if: $CI_PIPELINE_SOURCE == 'schedule'
    - if: $CI_COMMIT_TAG
  script:
    - yarn --frozen-lockfile
    - yarn install-chrome-dependencies
    - yarn lhci collect
    - yarn lhci assert
  artifacts:
    expose_as: Lighthouse reports
    when: always
    paths:
      - .lighthouseci
