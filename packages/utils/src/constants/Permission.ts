/**
 * A permission a user may have within an organization because of their given role.
 */
export enum Permission {
  ViewMembers,
  ViewApps,
  ManageRoles,
  ManageMembers,
  PublishBlocks,
  CreateApps,
  EditApps,
  EditAppMessages,
  EditAppSettings,
  DeleteApps,
  PushNotifications,
  ManageResources,
  InviteMember,
  EditOrganization,
  ManageTeams,
}
