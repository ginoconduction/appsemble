import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  submit: 'Save',
  uploadError: 'Something went wrong when trying to upload messages for this language.',
  uploadSuccess: 'Uploaded messages successfully',
});
