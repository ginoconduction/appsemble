import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  appLogo: 'App logo',
  description: 'Description',
  view: 'View App',
  readMore: 'Read more',
  readLess: 'Read less',
});
