import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  title: 'Create new organization',
  createButton: 'Create new organization',
  cancelLabel: 'Cancel',
  organizationId: 'Organization ID',
  organizationName: 'Organization name',
  description: 'Description',
  email: 'Email',
  website: 'Website',
});
