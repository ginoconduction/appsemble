export const {
  apiUrl,
  blockManifests,
  definition,
  id: appId,
  languages,
  logins,
  sentryDsn,
  sentryEnvironment,
  showAppsembleLogin,
  vapidPublicKey,
} = window.settings;
delete window.settings;
