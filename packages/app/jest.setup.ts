window.settings = {
  apiUrl: 'https://appsemble.dev',
  blockManifests: [],
  definition: null,
  id: 42,
  languages: ['en', 'nl'],
  logins: [],
  sentryDsn: null,
  sentryEnvironment: null,
  showAppsembleLogin: true,
  vapidPublicKey: '123',
};
